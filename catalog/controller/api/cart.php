<?php
class ControllerApiCart extends Controller {
	public function add() {
		$this->load->language('api/cart');
		$recurring_id = 0;
		$json = array();
		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->post['product'])) {
				$layer_price = array();
				if(isset($this->request->get['order_id'])) {
					foreach ($this->request->post['product'] as $product) {
						$layer_prices = $this->cart->getOrderLayerPrice($this->request->get['order_id'], $product['product_id']);
						if($layer_prices) {
							$layer_price[] = $layer_prices['layer_price'];
						}
					}
				} else {
					foreach ($this->request->post['product'] as $product) {
						$layer_prices = $this->cart->getLayerPrice($product['product_id']);
						if($layer_prices) {
							$layer_price[] = $layer_prices['layer_price'];
						}
					}
				}
				$this->cart->clear();
				foreach ($this->request->post['product'] as $key => $product) {
					if (isset($product['option'])) {
						$option = $product['option'];
					} else {
						$option = array();
					}
					if(isset($this->request->post['receipt_date'])) {
						$receipt_date = $this->request->post['receipt_date'];
					} else {
						$receipt_date = "";
					}
					$this->cart->add($product['product_id'], $product['quantity'], $option, $recurring_id, $receipt_date, $layer_price[$key]);
				}
				$json['success'] = $this->language->get('text_success');

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
			} elseif (isset($this->request->post['product_id'])) {
				$this->load->model('catalog/product');

				$product_info = $this->model_catalog_product->getProduct($this->request->post['product_id']);
				
				if ($product_info) {
					if (isset($this->request->post['quantity'])) {
						$quantity = $this->request->post['quantity'];
					} else {
						$quantity = 1;
					}
					
					if (isset($this->request->post['option'])) {
						$option = array_filter($this->request->post['option']);
					} else {
						$option = array();
					}
					$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
					foreach ($product_options as $product_option) {
						if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
							$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
						}
					}
					if (!isset($json['error']['option'])) {
						$this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id, $this->request->post['receipt_date']);

						$json['success'] = $this->language->get('text_success');

						unset($this->session->data['shipping_method']);
						unset($this->session->data['shipping_methods']);
						unset($this->session->data['payment_method']);
						unset($this->session->data['payment_methods']);
					}
				} else {
					$json['error']['store'] = $this->language->get('error_store');
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function edit() {
		$this->load->language('api/cart');
		$json = array();
		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$this->cart->update($this->request->post['key'], $this->request->post['quantity']);

			$json['success'] = $this->language->get('text_success');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function remove() {
		$this->load->language('api/cart');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			// Remove
			if (isset($this->request->post['key'])) {
				$this->cart->remove($this->request->post['key']);

				unset($this->session->data['vouchers'][$this->request->post['key']]);

				$json['success'] = $this->language->get('text_success');

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				unset($this->session->data['reward']);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function products() {
		$this->load->language('api/cart');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else {
			// Stock
			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$json['error']['stock'] = $this->language->get('error_stock');
			}

			// Products
			$json['products'] = array();
			$days = array();
			$receipt_date_array = array();
			$products = $this->cart->getProducts();
			foreach ($products as $product) {
				$product_total = 0;
				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$json['error']['minimum'][] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}
				$option_data = array();
				foreach ($product['option'] as $key => $option) {
					if(isset($option['product_option_id'])){
						if($option['type'] == "checkbox") {
							$option_data[] = array(
								'product_option_id'       => $option['product_option_id'],
								'product_option_value_id' => $option['product_option_value_id'],
								'name'                    => $option['name'],
								'value'                   => $option['value'],
								'type'                    => $option['type'],
								'pound_specific_option'   => $option['pound_specific_option'],
								'key_num'                 => $option['key_num']
							);
						} else {
							$option_data[] = array(
								'product_option_id'       => $option['product_option_id'],
								'product_option_value_id' => $option['product_option_value_id'],
								'name'                    => $option['name'],
								'value'                   => $option['value'],
								'type'                    => $option['type'],
								'pound_specific_option'   => $option['pound_specific_option']
							);
						}
					}
					
				}
				$json['products'][] = array(
					'cart_id'    => $product['cart_id'],
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => $option_data,
					'quantity'   => $product['quantity'],
					'stock'      => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'shipping'   => $product['shipping'],
					'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
					'total'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']),
					'reward'     => $product['reward'],
					'layer'      => $product['layer'] >= 1 ? $product['layer'] : 1
				);
				if($product['production_time']) {
					$days[] = $product['production_time'];
				}
				if($product['receipt_date']) {
					$receipt_date_array[] = strtotime($product['receipt_date']);
				}
			}
			// Voucher
			$json['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$json['vouchers'][] = array(
						'code'             => $voucher['code'],
						'description'      => $voucher['description'],
						'from_name'        => $voucher['from_name'],
						'from_email'       => $voucher['from_email'],
						'to_name'          => $voucher['to_name'],
						'to_email'         => $voucher['to_email'],
						'voucher_theme_id' => $voucher['voucher_theme_id'],
						'message'          => $voucher['message'],
						'price'            => $this->currency->format($voucher['amount'], $this->session->data['currency']),			
						'amount'           => $voucher['amount']
					);
				}
			}

			// Totals
			$this->load->model('setting/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array. 
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			$sort_order = array();

			$results = $this->model_setting_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get('total_' . $result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);
					
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}
			
			if($receipt_date_array) {
				$receipt_date_strtotime = max($receipt_date_array);//获取最大时间戳
			} else {
				$receipt_date_strtotime = "";
			}
			
			$sort_order = array();
			$ebd = 0;
			$first_order_discount_price = 0;
			$shipping = 0;
			$first_order_discount = 100;
			$days = array();
			$receipt_date = "";
			$cgds = 0;
			/*推荐用户首单*/
			$custromer_points = 0;
			$points_rule = 0;
			$custromer_ref = $this->cart->getCustomerReferrer();//查询用户是否是推荐用户
			if($custromer_ref) {
				if($custromer_ref['referrer_id']) {
					$custromer_points = $custromer_ref['referrer_id'];
					$order_num = $this->cart->getOrderList();//查询用户是否为首单
					if($order_num['num'] == 0) {
						$recommend_rule = $this->cart->getRecommendRule();//查询推荐规则
						if($recommend_rule) {
							$points_rule = $recommend_rule['reward'];
							$first_order_discount = $recommend_rule['discount'];//推荐用户首单优惠折扣
						} else {
							$first_order_discount = 100;
						}
					}
				}
			}
			
			/*---end---*/
			if($days) {
				$max_number = max($days);
			} else {
				$max_number = 0;
			}
			
			if($receipt_date_strtotime) {
				$receipt_date = date("Y-m-d", $receipt_date_strtotime);
				$this->load->model('discount/discount');
				$discount = $this->model_discount_discount->getDiscount();
				if($discount) {
					$booking_discount = $discount['days'] + $max_number;
				} else{
					$booking_discount = 0;
				}
				if($receipt_date_strtotime > strtotime(date("Y-m-d", strtotime("$booking_discount day")))) {
					if($discount['number']<0 && $discount['number'] >100) {
						$early_booking_discount = 100;
					} else {
						$early_booking_discount = $discount['number'];
					}
				} else {
					$early_booking_discount = 100;
				}
			} else {
				$early_booking_discount = 100;
			}
			
			$order_data['receiptdate'] = $receipt_date;
			
			$this->load->model('integral/integral');
			
			$integral_price = $this->model_integral_integral->checkIntegralSession();
			
			if($integral_price) {
				$integral_val = $integral_price['integral'];
			} else {
				$integral_val = 0;
			}
			
			foreach ($total_data['totals'] as $key => $value) {
				if($value['code'] == "shipping") {
					$shipping = $value['value'];
				}
				if($value['code'] == "coupon") {
					if($value['value'] == -($shipping)) {
						$shipping = 0;
					}
				}
				if($value['code'] == "sub_total") {
					$cgd = $this->cart->getCustomerGroupDiscount();//客户级别优惠折扣
					if($cgd) {$cgds = $value['value'] * (100 - $cgd) / 100;} else {$cgds = 0;}//客户级别优惠金额
				}
				if($value['code'] == "total") {
					$value['value'] = $value['value'] + $shipping * (100 - $cgd) / 100;
					$first_order_discount_price = ($value['value'] - $shipping) * (100 - $first_order_discount) / 100;//推荐首单优惠
					$ebd = ($value['value'] - $shipping) * (100 - $early_booking_discount) / 100 * $first_order_discount / 100;//早订优惠金额
					$totals[$key]['value'] = ($value['value'] - $shipping) * $first_order_discount / 100 * $early_booking_discount / 100  - $integral_val + $shipping;//总价 = 计算后的价格 * 首单优惠折扣 / 100 * 早订优惠折扣 / 100 - 积分抵扣
				    $sy = $totals[$key]['value'];
				}
				$sort_order[$key] = $value['sort_order'];
			}
			
			array_multisort($sort_order, SORT_ASC, $totals);
			
			$json['totals'] = array();
			if($first_order_discount_price) {
				$json['first_order_discount_price'] = $this->currency->format("-" . $first_order_discount_price, $this->session->data['currency']);
			} else {
				$json['first_order_discount_price'] = "";
			}
			
			if($ebd) {
				$json['early_booking_discount'] = $this->currency->format("-" . $ebd, $this->session->data['currency']);
			} else {
				$json['early_booking_discount'] = "";
			}
			
			if($cgds) {
				$json['customer_group_discount'] = $this->currency->format("-" . $cgds, $this->session->data['currency']);
			} else {
				$json['customer_group_discount'] = "";
			}
			
			foreach ($totals as $total) {
				$json['totals'][] = array(
					'title' => $total['title'],
					'code'  => $total['code'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
