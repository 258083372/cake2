<?php
class ControllerAccountCancel extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/cancel', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/cancel');
		
		$this->document->setTitle($this->language->get('heading_title'));

				$data['heading_title'] = $this->language->get('heading_title');
			

				$data['text_empty'] = $this->language->get('text_empty');
			

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/cancel', $url, true)
		);

		$this->load->model('account/cancel');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['cancels'] = array();

		$cancel_total = $this->model_account_cancel->getTotalCancels();
		$results = $this->model_account_cancel->getCancels(($page - 1) * 10, 10);

		foreach ($results as $result) {
			$data['cancels'][] = array(
				'cancel_id'  => $result['cancel_id'],
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'href'       => $this->url->link('account/cancel/info', 'cancel_id=' . $result['cancel_id'] . $url, true)
			);
		}

		$pagination = new Pagination();
		$pagination->total = $cancel_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
		$pagination->url = $this->url->link('account/cancel', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($cancel_total) ? (($page - 1) * $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit')) > ($cancel_total - $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit'))) ? $cancel_total : ((($page - 1) * $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit')) + $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit')), $cancel_total, ceil($cancel_total / $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit')));

		$data['continue'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/cancel_list', $data));
	}

	public function info() {
		$this->load->language('account/cancel');
		$data['text_order_id'] = $this->language->get('text_order_id');
		if (isset($this->request->get['cancel_id'])) {
			$cancel_id = $this->request->get['cancel_id'];
		} else {
			$cancel_id = 0;
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/cancel/info', 'cancel_id=' . $cancel_id, true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->model('account/cancel');

		$cancel_info = $this->model_account_cancel->getCancel($cancel_id);
		
		if ($cancel_info) {
			$this->document->setTitle($this->language->get('text_cancel'));

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/cancel', $url, true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_cancel'),
				'href' => $this->url->link('account/cancel/info', 'cancel_id=' . $this->request->get['cancel_id'] . $url, true)
			);

			$data['cancel_id'] = $cancel_info['cancel_id'];
			$data['order_id'] = $cancel_info['order_id'];
			$data['date_ordered'] = date($this->language->get('date_format_short'), strtotime($cancel_info['date_ordered']));
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($cancel_info['date_added']));
			$data['firstname'] = $cancel_info['firstname'];
			$data['lastname'] = $cancel_info['lastname'];
			$data['email'] = $cancel_info['email'];
			$data['telephone'] = $cancel_info['telephone'];
			$data['email_screenshots'] = $cancel_info['email_screenshots'];
			$data['whatsapp_screenshots'] = $cancel_info['whatsapp_screenshots'];
			$data['proof_of_refund'] = $cancel_info['proof_of_refund'];
			$data['status'] = $cancel_info['status'];
			$data['reason'] = $cancel_info['reason'];
			$data['comment'] = nl2br($cancel_info['comment']);
			$data['action'] = $cancel_info['action'];

			$data['histories'] = array();

			$results = $this->model_account_cancel->getCancelHistories($this->request->get['cancel_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => nl2br($result['comment'])
				);
			}

			$data['continue'] = $this->url->link('account/cancel', $url, true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/cancel_info', $data));
		} else {
			$this->document->setTitle($this->language->get('text_cancel'));

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/cancel', '', true)
			);

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_cancel'),
				'href' => $this->url->link('account/cancel/info', 'cancel_id=' . $cancel_id . $url, true)
			);

			$data['continue'] = $this->url->link('account/cancel', '', true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function add() {
		$this->load->language('account/cancel');

		$this->load->model('account/cancel');
		
		$this->load->model('account/order');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$production_time = $this->model_account_order->getProductionTime($this->request->post['order_id']);
			if($production_time) {
				$production_began_strtotime = strtotime("-" . $production_time['max_time'] . " day",strtotime($production_time['receipt_date']));
				if($production_began_strtotime) {
					if(strtotime(date('Y-m-d H:i:s')) < $production_began_strtotime) {
						/*写入图片*/
						$files_whatsapp = array();
						$whatsapp_screenshots = "";
						if (!empty($this->request->files['whatsapp']['name'])) {
							$directory_whatsapp = DIR_IMAGE . 'catalog/whatsapp';
							$files_whatsapp[] = array(
								'name'     => $this->request->files['whatsapp']['name'],
								'type'     => $this->request->files['whatsapp']['type'],
								'tmp_name' => $this->request->files['whatsapp']['tmp_name'],
								'error'    => $this->request->files['whatsapp']['error'],
								'size'     => $this->request->files['whatsapp']['size']
							);
							foreach ($files_whatsapp as $file) {
								if (is_file($file['tmp_name'])) {
									$filename_whatsapp = basename(html_entity_decode(md5(strtotime(date('Y-m-d H:i:s'))."whatsapp").substr($file['name'],strrpos($file['name'],".")), ENT_QUOTES, 'UTF-8'));
									move_uploaded_file($file['tmp_name'], $directory_whatsapp . '/' . $filename_whatsapp);
									$whatsapp_screenshots = "image/catalog/whatsapp/" . $filename_whatsapp;
								}
							}
						}
						$files_email_screenshots = array();
						$email_screenshots = "";
						if (!empty($this->request->files['email_screenshots']['name'])) {
							$directory_email_screenshots = DIR_IMAGE . 'catalog/email';
							$files_email_screenshots[] = array(
								'name'     => $this->request->files['email_screenshots']['name'],
								'type'     => $this->request->files['email_screenshots']['type'],
								'tmp_name' => $this->request->files['email_screenshots']['tmp_name'],
								'error'    => $this->request->files['email_screenshots']['error'],
								'size'     => $this->request->files['email_screenshots']['size']
							);
							foreach ($files_email_screenshots as $file) {
								if (is_file($file['tmp_name'])) {
									$filename_email = basename(html_entity_decode(md5(strtotime(date('Y-m-d H:i:s'))."email").substr($file['name'],strrpos($file['name'],".")), ENT_QUOTES, 'UTF-8'));
									move_uploaded_file($file['tmp_name'], $directory_email_screenshots . '/' . $filename_email);
									$email_screenshots = "image/catalog/email/" . $filename_email;
								}
							}
						}
						
						/* end 写入图片*/
						$this->model_account_cancel->addCancel($this->request->post, $whatsapp_screenshots, $email_screenshots);
						$this->response->redirect($this->url->link('account/cancel/success', '', true));
					}
				}
			}
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');
	

		$data['text_empty'] = $this->language->get('text_empty');
			
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/cancel/add', '', true)
		);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['order_id'])) {
			$data['error_order_id'] = $this->error['order_id'];
		} else {
			$data['error_order_id'] = '';
		}

		if (isset($this->error['firstname'])) {
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = '';
		}

		$data['action'] = $this->url->link('account/cancel/add', '', true);
		
		$cancel = "";
		if (isset($this->request->get['order_id'])) {
			$order_info = $this->model_account_order->getOrder($this->request->get['order_id']);
			$production_time = $this->model_account_order->getProductionTime($this->request->get['order_id']);
			$production_began_strtotime = strtotime("-" . $production_time['max_time'] . " day",strtotime($production_time['receipt_date']));
			if($production_began_strtotime) {
				if(strtotime(date('Y-m-d H:i:s')) < $production_began_strtotime) {
					$cancel = "1";
				}
			}
		}
		
		$data['cancel'] = $cancel;
		
		if (isset($this->request->post['order_id'])) {
			$data['order_id'] = $this->request->post['order_id'];
		} elseif (!empty($order_info)) {
			$data['order_id'] = $order_info['order_id'];
		} else {
			$data['order_id'] = '';
		}
		
		if (isset($this->request->post['date_ordered'])) {
			$data['date_ordered'] = $this->request->post['date_ordered'];
		} elseif (!empty($order_info)) {
			$data['date_ordered'] = date('Y-m-d', strtotime($order_info['date_added']));
		} else {
			$data['date_ordered'] = '';
		}

		if (isset($this->request->post['firstname'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} elseif (!empty($order_info)) {
			$data['firstname'] = $order_info['firstname'];
		} else {
			$data['firstname'] = $this->customer->getFirstName();
		}

		if (isset($this->request->post['lastname'])) {
			$data['lastname'] = $this->request->post['lastname'];
		} elseif (!empty($order_info)) {
			$data['lastname'] = $order_info['lastname'];
		} else {
			$data['lastname'] = $this->customer->getLastName();
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif (!empty($order_info)) {
			$data['email'] = $order_info['email'];
		} else {
			$data['email'] = $this->customer->getEmail();
		}
		
		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} elseif (!empty($order_info)) {
			$data['telephone'] = $order_info['telephone'];
		} else {
			$data['telephone'] = $this->customer->getTelephone();
		}

		if (isset($this->request->post['cancel_reason_id'])) {
			$data['cancel_reason_id'] = $this->request->post['cancel_reason_id'];
		} else {
			$data['cancel_reason_id'] = '';
		}

		$this->load->model('localisation/cancel_reason');
		$data['cancel_reasons'] = $this->model_localisation_cancel_reason->getCancelReasons();
		if (isset($this->request->post['comment'])) {
			$data['comment'] = $this->request->post['comment'];
		} else {
			$data['comment'] = '';
		}

		// Captcha
		if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('cancel', (array)$this->config->get('config_captcha_page'))) {
			$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
		} else {
			$data['captcha'] = '';
		}

		if ($this->config->get('config_cancel_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_cancel_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_cancel_id'), true), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}
		
		$data['back'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('account/cancel_form', $data));
		
	}

	protected function validate() {
		if (!$this->request->post['order_id']) {
			$this->error['order_id'] = $this->language->get('error_order_id');
		}

		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
		
		if (empty($this->request->post['cancel_reason_id'])) {
			$this->error['reason'] = $this->language->get('error_reason');
		}

		if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('cancel', (array)$this->config->get('config_captcha_page'))) {
			$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

			if ($captcha) {
				$this->error['captcha'] = $captcha;
			}
		}

		if ($this->config->get('config_cancel_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_cancel_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		return !$this->error;
	}

	public function success() {
		$this->load->language('account/cancel');

		$this->document->setTitle($this->language->get('heading_title'));

				$data['heading_title'] = $this->language->get('heading_title');
			

				$data['text_empty'] = $this->language->get('text_empty');
			

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/cancel', '', true)
		);

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}