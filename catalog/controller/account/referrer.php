<?php
class ControllerAccountReferrer extends Controller {
	private $error = array();

	public function index() {
	    if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/referrer', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
		$this->load->model('account/customer');
		$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
		
		if($customer_info['email']) {
		    $data['referrer_email'] = $this->url->link('account/register&&referrer_email='.$customer_info['email'], '', true);
		}
		
		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');
			

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_referrer'),
			'href' => $this->url->link('account/referrer', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
    
		$this->response->setOutput($this->load->view('account/referrer', $data));
	}
}