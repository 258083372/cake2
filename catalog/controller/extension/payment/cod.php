<?php
class ControllerExtensionPaymentCod extends Controller {
	public function index() {
		return $this->load->view('extension/payment/cod');
	}

	public function confirm() {
		$json = array();
		
		if ($this->session->data['payment_method']['code'] == 'cod') {
			$this->load->model('checkout/order');
			$this->load->model('integral/integral');
			$get_integral = $this->model_integral_integral->getIntegralLists();
			if(!isset($get_integral['zero'])) {
				$check_integral_session = $this->model_integral_integral->checkIntegralSession();
				if($check_integral_session) {
					$value = $check_integral_session['integral'] / $get_integral['ratio'];
				} else {
					$value = "";
				}
				$this->model_integral_integral->addCustomerReward($this->session->data['order_id'], $value);
			}
			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_cod_order_status_id'));
			$json['redirect'] = $this->url->link('checkout/success');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
	}
}
