<?php
// Heading
$_['heading_title']      = 'Product Cancels';

// Text
$_['text_account']       = 'Account';
$_['text_cancel']        = 'Cancel Information';
$_['text_cancel_detail'] = 'Cancel Details';
$_['text_description']   = 'Please complete the form below to request an RMA number.';
$_['text_order']         = 'Order Information';
$_['text_reason']        = 'Reason for Cancel';
$_['text_message']       = '<p>Thank you for submitting your cancel request. Your request has been sent to the relevant department for processing.</p><p> You will be notified via e-mail as to the status of your request.</p>';
$_['text_cancel_id']     = 'Cancel ID:';
$_['text_order_id']      = 'Order ID:';
$_['text_date_ordered']  = 'Order Date:';
$_['text_status']        = 'Status:';
$_['text_date_added']    = 'Date Added:';
$_['text_comment']       = 'Cancel Comments';
$_['text_history']       = 'Cancel History';
$_['text_empty']         = 'You have not made any previous cancels!';
$_['text_agree']         = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['text_whatsapp']      = 'WhatsApp screenshots';
$_['text_email_screenshots'] = 'Email screenshots';
$_['text_proof_of_refund'] = 'Proof of Refund';

// Column
$_['column_cancel_id']   = 'Cancel ID';
$_['column_order_id']    = 'Order ID';
$_['column_status']      = 'Status';
$_['column_date_added']  = 'Date Added';
$_['column_customer']    = 'Customer';
$_['column_price']       = 'Price';
$_['column_comment']     = 'Comment';
$_['column_reason']      = 'Reason';
$_['column_action']      = 'Action';

// Entry
$_['entry_order_id']     = 'Order ID';
$_['entry_date_ordered'] = 'Order Date';
$_['entry_firstname']    = 'First Name';
$_['entry_lastname']     = 'Last Name';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Telephone';
$_['entry_reason']       = 'Reason for Cancel';
$_['entry_fault_detail'] = 'Faulty or other details';

// Error
$_['text_error']         = 'The cancels you requested could not be found!';
$_['error_order_id']     = 'Order ID required!';
$_['error_firstname']    = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 1 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']    = 'Telephone must be between 3 and 32 characters!';
$_['error_reason']       = 'You must select a cancel product reason!';
$_['error_agree']        = 'Warning: You must agree to the %s!';