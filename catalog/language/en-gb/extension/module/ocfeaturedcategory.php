<?php
// Heading
$_['heading_title'] = 'Featured category';
$_['text_empty_featurer_category'] = 'There is no featured category';
$_['module_description'] = 'Featured category allows to display selected on your homepage';
$_['text_viewall'] = 'View All';