<?php
// Text
$_['text_home']          = '主頁';
$_['text_wishlist']      = '收藏(%s)';
$_['text_shopping_cart'] = '購物車';
$_['text_category']      = '商品分類';
$_['text_account']       = '我的帳戶';
$_['text_register']      = '註冊';
$_['text_login']         = '登錄';
$_['text_login2']        = '<strong>登錄</strong>或加入我們的網站';
$_['text_order']         = '購買歷史';
$_['text_transaction']   = '電子扣帳額';
$_['text_download']      = '下載';
$_['text_logout']        = '登出';
$_['text_checkout']      = '結帳';
$_['text_search']        = '搜尋';
$_['text_all']           = '顯示全部';
$_['text_phone']         = '我們的惡電話: ';
$_['text_setting']       = '設置';
