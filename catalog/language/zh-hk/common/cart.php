<?php
// Text
$_['text_items']     = '<span class="txt_number">%s</span><span class="txt_items">件</span><span class="total-price">%s</span>';
$_['text_empty']     = '您的購物車內沒有商品！';
$_['text_cart']      = '我的購物車';
$_['text_checkout']  = '結帳';
$_['text_recurring'] = '定期付款';
