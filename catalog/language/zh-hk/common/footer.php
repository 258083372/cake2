<?php
// Text
$_['text_information']  = '資訊';
$_['text_service']      = '客戶服務';
$_['text_extra']        = '其他資訊';
$_['text_contact']      = '連絡我們';
$_['text_return']       = '退換服務';
$_['text_sitemap']      = '網站地圖';
$_['text_manufacturer'] = '品牌專區';
$_['text_voucher']      = '電子禮券';
$_['text_affiliate']    = '聯盟行銷計劃';
$_['text_special']      = '特價商品';
$_['text_account']      = '會員中心';
$_['text_order']        = '歷史訂單';
$_['text_wishlist']     = '收藏清單';
$_['text_newsletter']   = '訂閱本站資訊';
$_['text_powered']      = 'Copyright &copy; 2018 <a href="#">Cake</a>, Inc. All Right Reserved.';
$_['text_backtop']      = '<span>回到顶部</span><i class="ion-chevron-right"></i><i class="ion-chevron-right"></i>';