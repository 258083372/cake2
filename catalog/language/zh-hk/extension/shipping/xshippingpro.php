<?php
// Text
$_['text_title']       = '運輸選項';

$_['xshippingpro_estimator_header'] = '估計運費';
$_['xshippingpro_estimator_tab'] = '運輸成本';
$_['xshippingpro_estimator_country'] = '選擇送貨的國家';
$_['xshippingpro_estimator_zone'] = '選擇送貨的區域';
$_['xshippingpro_estimator_postal'] = '請輸入郵政編碼';
$_['xshippingpro_estimator_no_data'] = '沒有符合條件的運輸方式可用';
$_['xshippingpro_estimator_button'] = '運輸成本';
$_['xshippingpro_available'] = '是的，我們把我們的產品運送到你的位置!';
$_['xshippingpro_no_available'] = '對不起，我們不運送到您的位置!';

$_['xshippingpro_select'] = '-請選擇-';
$_['xshippingpro_select_error'] = '請選擇一個項目';
$_['ocm_months'] = '一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月';
$_['ocm_months_short'] = '一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月c';
$_['ocm_weekdays'] = '星期天_星期一_星期二_星期三_星期四_星期五_星期六';
$_['ocm_weekdays_short'] = '星期天_星期一_星期二_星期三_星期四_星期五_星期六';