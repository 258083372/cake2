<?php
// Heading 
$_['heading_title'] 	 = '通訊';
$_['heading_title2'] 	 = 'Ten Percent Member Discount';
$_['heading_title3'] 	 = 'Special Ofers For Subscribers';
$_['text_colose'] 	 = '<i class="fa fa-close"></i>';

$_['newletter_des'] 	 = 'Subscribe to our newsletters now and stay up-to-date with new collections, the latest lookbooks and exclusive offers.';
$_['newletter_des2'] 	 = '聯取有關我們最新商店和特惠信息的電子郵件更新。';

//Fields
$_['entry_email'] 		 = '請輸入您的電子郵件';
$_['entry_name'] 		 = 'Name';

//Buttons
$_['entry_button'] 		 = '確定';
$_['entry_unbutton'] 	 = 'Unsubscribe';

//text
$_['text_subscribe'] 	 = 'Subscribe Here';

$_['mail_subject']   	 = 'NewsLetter Subscribe';

//Messages
$_['error_invalid'] 	 = 'Error : Please enter valid email address.';
$_['subscribe']	    	 = 'Success : Subscribed Successfully.';
$_['unsubscribe'] 	     = 'Success : Unsubscribed Successfully.';
$_['alreadyexist'] 	     = 'Error : Email already exist.';
$_['notexist'] 	    	 = 'Error : Email doesn\'t exist.';
$_['entry_show_again']   =  "Don't show this popup again";