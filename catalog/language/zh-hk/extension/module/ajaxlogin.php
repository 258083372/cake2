<?php

// Heading
$_['heading_title']                = 'Ajax 登錄';

// Text
$_['text_account']                 = '賬戶';
$_['text_login']                   = '登錄';
$_['text_new_customer']            = '新用戶';
$_['text_register']                = '註冊用戶';
$_['text_register_account']        = '通過創建一個帳戶，您將能夠更快地購物，是最新的訂單的狀態，並保持跟蹤您以前的訂單。';
$_['text_returning_customer']      = '';
$_['text_i_am_returning_customer'] = '';
$_['text_forgotten']               = '忘記密碼';
$_['button_register_link']         = '創建一個賬戶';
$_['success_message']              = '恭喜!登錄成功';

// Entry
$_['entry_email']                  = '電子郵件地址';
$_['entry_password']               = '密碼';

// Error
$_['error_login']                  = '警告:沒有匹配的電子郵件地址和/或密碼。';
$_['error_attempts']               = '警告:您的帳戶已超過允許的登錄次數。請在1小時後再試。';
$_['error_approved']               = '警告:您的帳戶需要批准才能登錄。';