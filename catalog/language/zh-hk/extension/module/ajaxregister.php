<?php

// Heading
$_['heading_title']        = '註冊賬戶';

// Text
$_['text_account']         = '賬戶';
$_['text_register']        = '註冊';
$_['text_account_already'] = '如果你已經有我們的賬戶，請登錄在<a onclick="ocajaxlogin.appendLoginForm()" href="javascript:void(0);">登錄表單</a>.';
$_['text_your_details']    = '你的個人資料';
$_['text_your_address']    = '你的地址';
$_['text_newsletter']      = '通訊';
$_['text_your_password']   = '你的密碼';
$_['text_agree']           = '本人已閱讀並同意<a href="%s" class="agree"><b>%s</b></a>';
$_['text_referrer']        = '推薦人';
$_['text_referrer_email']  = '推薦人電郵';

// Entry
$_['entry_customer_group'] = '客戶羣體';
$_['entry_firstname']      = '姓';
$_['entry_lastname']       = '名';
$_['entry_email']          = '電子郵件';
$_['entry_telephone']      = '電話';
$_['entry_fax']            = '傳真';
$_['entry_company']        = '公司';
$_['entry_address_1']      = '地址1';
$_['entry_address_2']      = '地址2';
$_['entry_postcode']       = '郵政編碼';
$_['entry_city']           = '城市';
$_['entry_country']        = '國家';
$_['entry_zone']           = '地區';
$_['entry_newsletter']     = '訂閱';
$_['entry_password']       = '密碼';
$_['entry_confirm']        = '確認密碼';

// Error
$_['error_exists']         = '警告:電子郵件地址已註冊!';
$_['error_firstname']      = '姓必須在1到32個字符之間!';
$_['error_lastname']       = '名必須在1到32個字符之間!';
$_['error_email']          = '電子郵件地址無效!';
$_['error_telephone']      = '電話必須在3到32個字符之間!';
$_['error_address_1']      = '地址1必須在3到128個字符之間!';
$_['error_city']           = '地址2必須在3到128個字符之間!';
$_['error_postcode']       = '郵政編碼必須在2到10個字符之間!';
$_['error_country']        = '請選擇一個國家!';
$_['error_zone']           = '請選擇一個地區/州!';
$_['error_custom_field']   = '這個字段是必填的!';
$_['error_password']       = '密碼必須在4到20個字符之間!';
$_['error_confirm']        = '密碼確認不匹配密碼!';
$_['error_agree']          = '警告:您必須同意%s!';