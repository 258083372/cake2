<?php
// Heading
$_['heading_title']      = '退換商品';

// Text
$_['text_account']       = '會員帳號';
$_['text_return']        = '退換貨信息';
$_['text_return_detail'] = '退換貨詳情';
$_['text_description']   = '<p>請填寫下表以取得 RMA 號碼。</p>';
$_['text_order']         = '訂單信息';
$_['text_reason']        = '退換貨品之原因';
$_['text_message']       = '<p>您的商品退換請求已經發送到相關部門處理。</p><p> 我們將通過電子 郵件通知您退換貨的作業進度。</p>';
$_['text_return_id']     = '退換 ID';
$_['text_order_id']      = '訂單 ID';
$_['text_date_ordered']  = '訂單日期';
$_['text_status']        = '狀態';
$_['text_date_added']    = '新增日期';
$_['text_comment']       = '退換備註';
$_['text_history']       = '退換歷史記錄';
$_['text_empty']         = '您目前沒有商品退換記錄。';
$_['text_agree']         = '我已經閱讀並同意<a href="%s"  class="agree"><b>%s</b></a>條款。';
$_['text_whatsapp']      = 'WhatsApp 截圖';
$_['text_email_screenshots'] = 'Email 截圖';
$_['text_proof_of_refund'] = '退款憑證';

// Column
$_['column_return_id']   = '退換 ID';
$_['column_order_id']    = '訂單 ID';
$_['column_status']      = '狀態';
$_['column_date_added']  = '新增日期';
$_['column_customer']    = '客戶';
$_['column_price']       = '價格';
$_['column_comment']     = '備註';
$_['column_reason']      = '原因';
$_['column_action']      = '管理';

// Entry
$_['entry_order_id']     = '訂單 ID';
$_['entry_date_ordered'] = '訂單日期';
$_['entry_firstname']    = '名字';
$_['entry_lastname']     = '姓氏';
$_['entry_email']        = '電子郵箱';
$_['entry_telephone']    = '電話';
$_['entry_reason']       = '退換原因';
$_['entry_fault_detail'] = '商品瑕疵或其它';

// Error
$_['text_error']         = '找不到您查詢的退換記錄。';
$_['error_order_id']     = '請輸入訂單 ID。';
$_['error_firstname']    = '名字必須在 1 至 32 個字元之間！';
$_['error_lastname']     = '姓氏必須在 1 至 32 個字元之間！';
$_['error_email']        = '無效的電子郵箱。';
$_['error_telephone']    = '電話號碼必須是 3 至 32 個字元之間！';
$_['error_reason']       = '請選擇一個退換原因。';
$_['error_agree']        = '警告：您必須同意%s條款！';
