<?php
class ModelCatalogCategory extends Model {
	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row;
	}

	public function getCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}

	public function getCategoryFilters($category_id) {
		$implode = array();

		$query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$implode[] = (int)$result['filter_id'];
		}

		$filter_group_data = array();

		if ($implode) {
			$filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

			foreach ($filter_group_query->rows as $filter_group) {
				$filter_data = array();

				$filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

				foreach ($filter_query->rows as $filter) {
					$filter_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name']
					);
				}

				if ($filter_data) {
					$filter_group_data[] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $filter_data
					);
				}
			}
		}

		return $filter_group_data;
	}

	public function getCategoryLayoutId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return (int)$query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row['total'];
	}
	
	public function getCategoryOptions($category_id) {
		$category_option_data = array();
	
		$category_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.category_id = '" . (int)$category_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");
	
		foreach ($category_option_query->rows as $category_option) {
			$category_option_value_data = array();
	
			$category_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.category_id = '" . (int)$category_id . "' AND pov.category_option_id = '" . (int)$category_option['category_option_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order");
	
			foreach ($category_option_value_query->rows as $category_option_value) {
				$category_option_value_data[] = array(
					'category_option_value_id' => $category_option_value['category_option_value_id'],
					'option_value_id'         => $category_option_value['option_value_id'],
					'name'                    => $category_option_value['name'],
					'image'                   => $category_option_value['image'],
					'quantity'                => $category_option_value['quantity'],
					'subtract'                => $category_option_value['subtract'],
					'price'                   => $category_option_value['price'],
					'min'                     => $category_option_value['min'],
					'max'                     => $category_option_value['max'],
					'step'                    => $category_option_value['step'],
					'price_prefix'            => $category_option_value['price_prefix'],
					'weight'                  => $category_option_value['weight'],
					'weight_prefix'           => $category_option_value['weight_prefix']
				);
			}
			
			if($category_option['type'] == "num") {
				$category_option_value_querya = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.category_id = '" . (int)$category_id . "' AND pov.category_option_id = '" . (int)$category_option['category_option_id'] . "' ORDER BY ov.sort_order");
				foreach ($category_option_value_querya->rows as $category_option_value) {
					$category_option_value_data[] = array(
						'category_option_value_id' => $category_option_value['category_option_value_id'],
						'option_value_id'         => $category_option_value['option_value_id'],
						'name'                    => $category_option_value['name'],
						'image'                   => $category_option_value['image'],
						'quantity'                => $category_option_value['quantity'],
						'subtract'                => $category_option_value['subtract'],
						'price'                   => $category_option_value['price'],
						'min'                     => $category_option_value['min'],
						'max'                     => $category_option_value['max'],
						'step'                    => $category_option_value['step'],
						'price_prefix'            => $category_option_value['price_prefix'],
						'weight'                  => $category_option_value['weight'],
						'weight_prefix'           => $category_option_value['weight_prefix']
					);
				}
			}
			
			$category_option_data[] = array(
				'category_option_id'    => $category_option['category_option_id'],
				'category_option_value' => $category_option_value_data,
				'option_id'            => $category_option['option_id'],
				'name'                 => $category_option['name'],
				'type'                 => $category_option['type'],
				'value'                => $category_option['value'],
				'required'             => $category_option['required'],
				'choose_up_layer'      => $category_option['choose_up_layer']
			);
		}
	
		return $category_option_data;
	}
	
}