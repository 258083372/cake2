<?php
class ModelExtensionTotalTotal extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/total');
		
		$query = $this->cart->getCustomerGroupDiscount();
		
		$total['totals'][] = array(
			'code'       => 'total',
			'title'      => $this->language->get('text_total'),
			'value'      => max(0, $total['total'] * $query / 100),
			'sort_order' => $this->config->get('total_total_sort_order')
		);
	}
}