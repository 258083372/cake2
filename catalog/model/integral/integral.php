<?php
class ModelIntegralIntegral extends Model {
	
	public function getLists($v) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "integration_rule WHERE minprice <= '" . $v . "'");
		return $query->rows;
	}
	
	public function getIntegralLists() {
		$query = $this->db->query("SELECT points FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$this->customer->getId() . "'")->rows;//check customer points
		$data = [];
		$querys = $this->db->query("SELECT * FROM ". DB_PREFIX ."integral")->row;
		if($querys) {
			if($querys["integral"] == "0" || $querys["price"] == "0") {
				$data["zero"] = "zero";
			} else {
				$data['points'] = array_sum(array_column($query, 'points'));//this customer points sum
				$ratio = $querys["price"] / $querys["integral"];
				$data['ratio'] = $ratio;
				$data['conversion'] = $ratio * $data['points'];
			}
		} else {
			$data["zero"] = "zero";
		}
		return $data;
	}
	
	public function add($integral_value) { 
		$integral_session_row = $this->db->query("SELECT COUNT(id) FROM " . DB_PREFIX . "integral_session WHERE customer_id = '" . (int)$this->customer->getId() . "'")->row;//check this customer yes or no have integral_session
		if($integral_session_row['COUNT(id)'] == 0) {
			$this->db->query("INSERT " . DB_PREFIX . "integral_session SET customer_id = '" . (int)$this->customer->getId() . "', integral = '" . (float)$integral_value . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "integral_session SET integral = '" . (float)$integral_value . "' WHERE customer_id = '" . (int)$this->customer->getId() ."'");
		}
	}
	
	public function checkIntegralSession() {
		$query = $this->db->query("SELECT integral FROM " . DB_PREFIX . "integral_session WHERE customer_id = '" . (int)$this->customer->getId() . "'")->row;
		return $query;
	}
	
	public function getIntegral() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "integral")->row;
		return $query;
	}
	
	public function addCustomerReward($order_id, $value) {
		if($value) {
			$this->db->query("INSERT " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$this->customer->getId() . "', order_id = '" . (int)$order_id . "', points = '-" . (int)$value . "', date_added = NOW()");
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "integral_session WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}
	
	public function delectIntegral() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "integral_session WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}
	
}
