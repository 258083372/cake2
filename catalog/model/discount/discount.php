<?php
class ModelDiscountDiscount extends Model {
	public function getDiscount() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "early_booking_discount")->row;
		return $query;
	}
	public function getCustomerGroup() {
		$customer_group = $this->db->query("SELECT gm.discount FROM " . DB_PREFIX . "customer as cm, " . DB_PREFIX . "customer_group as gm WHERE cm.customer_id = '" . (int)$this->customer->getId() . "' and cm.customer_group_id = gm.customer_group_id")->row;
		return $customer_group;
	}
}
