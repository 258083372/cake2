<?php

$_['d_social_login_facebook'] = array(
    "Facebook" => array(
        "enabled" => false,
        "name" => "Facebook",
        "keys" => array("id" => "", "secret" => ""),
        "scope" => 'email',
        "id" => 'facebook',
        "trustForwarded" => false,
        "sort_order" => 2,
        "icon" => 'facebook.svg',
        "background_color" => '#4864b4',
        "background_color_active" => '#3a5192',
        "background_color_hover" => '#3a5192',
        "documentation_url" => "https://doc.99logins.com/facebook",
    )
);
