<?php

$_['d_social_login_dribble'] = array(
    "Dribbble" => array (
        "enabled" => false,
        "name" => "Dribbble",
        "keys"    => array ( "id" => "", "secret" => "" ),
        "id"  => 'dribbble',
        "sort_order" => 14,
        "icon" => 'dribbble.svg',
        "background_color" => '#C32361',
        "background_color_active" => '#841640',
        "background_color_hover" => '#841640',
        "documentation_url" => "https://doc.99logins.com/dribbble",
    ),
);