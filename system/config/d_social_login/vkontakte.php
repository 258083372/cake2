<?php
$_['d_social_login_vkontakte'] = array(
    "Vkontakte" => array (
        "enabled" => false,
        "name" => "Vkontakte",
        "keys"    => array( "id" => "", "secret" => "" ),
        "id"  => 'vkontakte',
        "sort_order" => 6,
        "icon" => 'vkontakte.svg',
        "background_color" => '#5b82ab',
        "background_color_active" => '#466382',
        "background_color_hover" => '#466382',
        "documentation_url" => "https://doc.99logins.com/vkontakte",
    ),
);
