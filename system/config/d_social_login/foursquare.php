<?php

$_['d_social_login_foursquare'] = array(
    "Foursquare" => array (
        "enabled" => false,
        "name" => "Foursquare",
        "keys"    => array ( "id" => "", "secret" => "" ),
        "id"  => 'foursquare',
        "sort_order" => 12,
        "icon" => 'foursquare.svg',
        "background_color" => '#de5e5e',
        "background_color_active" => '#7c3232',
        "background_color_hover" => '#7c3232',
        "documentation_url" => "https://doc.99logins.com/foursquare",
    ),
);