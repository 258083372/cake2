<?php

$_['d_social_login_spotify'] = array(
    "Spotify" => array (
        "enabled" => false,
        "name" => "Spotify",
        "keys"    => array ( "id" => "", "secret" => "" ),
        "id"  => 'spotify',
        "sort_order" => 21,
        "icon" => 'spotify.svg',
        "background_color" => '#1DB954',
        "background_color_active" => '#12823a',
        "background_color_hover" => '#12823a',
        "documentation_url" => "https://doc.99logins.com/spotify",
    )
);
