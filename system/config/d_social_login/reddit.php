<?php

$_['d_social_login_reddit'] = array(
    "Reddit" => array (
        "enabled" => false,
        "name" => "Reddit",
        "keys"    => array ( "id" => "", "secret" => "" ),
        "id"  => 'reddit',
        "sort_order" => 20,
        "icon" => 'reddit.svg',
        "background_color" => '#FF4300',
        "background_color_active" => '#ED001C',
        "background_color_hover" => '#ED001C',
        "documentation_url" => "https://doc.99logins.com/reddit",
    )
);
