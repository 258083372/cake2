<?php

$_['d_social_login_tumblr'] = array(
    "Tumblr" => array (
        "enabled" => false,
        "name" => "Tumblr",
        "keys"    => array( "key" => "", "secret" => "" ),
        "id"  => 'tumblr',
        "sort_order" => 10,
        "icon" => 'tumblr.svg',
        "background_color" => '#365572',
        "background_color_active" => '#2f404f',
        "background_color_hover" => '#2f404f',
        "documentation_url" => "https://doc.99logins.com/tumblr",
    ),
);
