<?php

$_['d_social_login_github'] = array(
    "GitHub" => array (
        "enabled" => false,
        "name" => "Github",
        "keys"    => array ( "id" => "", "secret" => "" ),
        "id"  => 'github',
        "sort_order" => 13,
        "icon" => 'github.svg',
        "background_color" => '#333333',
        "background_color_active" => '#5A8BC9',
        "background_color_hover" => '#5A8BC9',
        "documentation_url" => "https://doc.99logins.com/github",
    ),
);