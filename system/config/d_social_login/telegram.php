<?php

$_['d_social_login_telegram'] = array(
    "Telegram" => array (
        "enabled" => false,
        "name" => "Telegram",
        "keys"    => array ( "id" => "", "secret" => "" ),
        "id"  => 'telegram',
        "sort_order" => 22,
        "icon" => 'telegram.svg',
        "background_color" => '#0088CC',
        "background_color_active" => '#0f5577',
        "background_color_hover" => '#0f5577',
        "documentation_url" => "https://doc.99logins.com/telegram",
    )
);
