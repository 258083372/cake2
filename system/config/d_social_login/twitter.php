<?php


$_['d_social_login_twitter'] = array(
    "Twitter" => array (
        "enabled" => true,
        "name" => "Twitter",
        "keys"    => array( "key" => "", "secret" => "" ),
        "id"  => 'twitter',
        "includeEmail" => true,
        "sort_order" => 3,
        "icon" => 'twitter.svg',
        "background_color" => '#00ceff',
        "background_color_active" => '#03b3dd',
        "background_color_hover" => '#03b3ed',
        "documentation_url" => "https://doc.99logins.com/twitter",
    ),
);
