<?php
$_['d_social_login_paypal'] = array(
    "Paypal" => array(
        "enabled" => false,
        "name" => "Paypal",
        "keys" => array("id" => "", "secret" => ""),
        "id" => 'paypal',
        "sort_order" => 8,
        "icon" => 'paypal.svg',
        "background_color" => '#179bd7',
        "background_color_active" => '#222d65',
        "background_color_hover" => '#222d65',
        "documentation_url" => "https://doc.99logins.com/paypal",
    )
);
