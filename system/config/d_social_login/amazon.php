<?php

$_['d_social_login_amazon'] = array(
    "Amazon" => array (
        "enabled" => false,
        "name" => "Amazon",
        "keys"    => array ( "id" => "", "secret" => "" ),
        "id"  => 'amazon',
        "sort_order" => 18,
        "icon" => 'amazon.svg',
        "background_color" => '#f0c14b',
        "background_color_active" => '#cea132',
        "background_color_hover" => '#cea132',
        "documentation_url" => "https://doc.99logins.com/amazon",
    )
);