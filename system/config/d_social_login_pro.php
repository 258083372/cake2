<?php
$_["d_social_login_pro"] = array(
    "available_providers" => [
        'facebook',
        'twitter',
        'vkontakte',
        'paypal',
        'tumblr',
        'foursquare',
        'github',
        'dribble',
        'amazon',
        'reddit',
        'spotify',
        'telegram'
    ]
);

