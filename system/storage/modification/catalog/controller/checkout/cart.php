<?php
class ControllerCheckoutCart extends Controller {
	public function index() {
		$this->load->language('checkout/cart');
		$this->load->model('integral/integral');
		$this->model_integral_integral->delectIntegral();
		$this->document->setTitle($this->language->get('heading_title'));

				$data['heading_title'] = $this->language->get('heading_title');
			

				$data['text_empty'] = $this->language->get('text_empty');
			

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('checkout/cart'),
			'text' => $this->language->get('heading_title')
		);

		if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$data['error_warning'] = $this->language->get('error_stock');
			} elseif (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$data['attention'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			$data['action'] = $this->url->link('checkout/cart/edit', '', true);
			$data['actions'] = $this->url->link('checkout/cart/editer', '', true);
			
			$data['action_integral'] = $this->url->link('checkout/cart/integral', '', true);

			if ($this->config->get('config_cart_weight')) {
				$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			} else {
				$data['weight'] = '';
			}

			$this->load->model('tool/image');
			$this->load->model('tool/upload');

			$data['products'] = array();
            $production_time = array();
            
			$products = $this->cart->getProducts();
			foreach ($products as $product) {
				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_height'));
				} else {
					$image = '';
				}

				$option_data = array();

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => $value
					);
				}

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
					
					$price = $this->currency->format($unit_price, $this->session->data['currency']);
					$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
				} else {
					$price = false;
					$total = false;
				}

				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year')
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}

				$data['products'][] = array(
					'cart_id'   => $product['cart_id'],
					'thumb'     => $image,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'recurring' => $recurring,
					'quantity'  => $product['quantity'],
					'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
					'price'     => $price,
					'total'     => $total,
					'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
				);
				$production_time[] = $product['production_time'];
			}
			if($production_time) {
			    $data['max_number'] = max($production_time);
			} else {
			    $data['max_number'] = 0;
			}

			// Gift Voucher
			$data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
						'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)
					);
				}
			}

			// Totals
			$this->load->model('setting/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_setting_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get('total_' . $result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}
			
			/*----積分----*/
						
			$get_integral = $this->model_integral_integral->getIntegralLists();
			if($get_integral) {
				if(isset($get_integral['zero'])) {
					$data['zero'] = "zero";
				} else {
					$data['integr'] = sprintf($this->language->get('text_integrals'), $get_integral['points']);
					$data['conversion'] = sprintf($this->language->get('text_conversion'), $get_integral['conversion']);
					$v = $totals[1]['value'];
					$integral = $this->model_integral_integral->getLists($v);
					$data['get_conversion'] = $get_integral['conversion'];
					
					$check_integral_session = $this->model_integral_integral->checkIntegralSession();
					if($check_integral_session) {
						if($get_integral['conversion'] < $check_integral_session['integral']) {
							$this->model_integral_integral->UpIntegralSession();
							$data['integral_session_val'] = 0;
						} else {
							if($check_integral_session) {
								$data['integral_session_val'] = $check_integral_session['integral'];
							} else {
								$data['integral_session_val'] = 0;
							}
						}
					} else {
						$data['integral_session_val'] = 0;
					}
					$data['other'] = "";
					if($integral) {
						$array_integrals_maximum = [];
						foreach ($integral as $integrals) {
							if($integrals['maximum'] <= $get_integral['conversion']) {
								$array_integrals_maximum[] = $integrals['maximum'];
							}
						}
						if(!in_array($data['integral_session_val'] ,$array_integrals_maximum) && $data['integral_session_val'] != 0) {
							$data['other'] = "-1";
						}
					}
					
					$data['integrals'] = $integral;
				}
				
			}
			
			/*----end積分----*/
			
			/*推荐用户首单*/
			$first_order_discount = 100;
			$custromer_ref = $this->cart->getCustomerReferrer();//查询用户是否是推荐用户
			if($custromer_ref['referrer_id']) {
				
				$order_num = $this->cart->getOrderList();//查询用户是否为首单
				if($order_num['num'] == 0) {
					$recommend_rule = $this->cart->getRecommendRule();//查询推荐规则
					if($recommend_rule) {
						$first_order_discount = $recommend_rule['discount'];//推荐用户首单优惠折扣
					}
				}
			}
			
			/*---end---*/
			$cgd = $this->cart->getCustomerGroupDiscount();
			$cgds = 0;
			$first_order_discount_price = 0;

			$data['totals'] = array();
			foreach ($totals as $key => $total) {
				$total['value'] = $total['value'];
				if($total['value'] < 0 ) {
					$total['value'] = 0;
				}
				if($total['code'] == 'sub_total' && $cgd != '100') {
					$cgds = $this->currency->format("-".($total['value'] - $total['value'] * $cgd / 100), $this->session->data['currency']);
				}
				if($total['code'] == 'total' && $first_order_discount != '100') {
					$first_order_discount_price = $this->currency->format("-".($total['value'] * (100 - $first_order_discount) / 100), $this->session->data['currency']);
					$total['value'] = $total['value'] * $first_order_discount / 100;
				}
				
				$data['totals'][] = array(
					'title' => $total['title'],
					'code'  => $total['code'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}
			
			$data['cgds'] = $cgds;
			
			$data['first_order_discount_price'] = $first_order_discount_price;
			            

			$data['continue'] = $this->url->link('common/home');

			$data['checkout'] = $this->url->link('checkout/checkout', '', true);

			$this->load->model('setting/extension');

			$data['modules'] = array();
			
			$files = glob(DIR_APPLICATION . '/controller/extension/total/*.php');

			if ($files) {
				foreach ($files as $file) {
					$result = $this->load->controller('extension/total/' . basename($file, '.php'));
					
					if ($result) {
						$data['modules'][] = $result;
					}
				}
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('checkout/cart', $data));
		} else {
			$data['text_error'] = $this->language->get('text_empty');
			
			$data['continue'] = $this->url->link('common/home');

			unset($this->session->data['success']);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function add() {
		$this->load->language('checkout/cart');
		

		$json = array();
		
		if(!$this->customer->getId()){
	        $json['error']['not_customer_id'] = "no login";
	    }

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (isset($this->request->post['quantity'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}

			if (!$json) {
				$this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id);

				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

				// Unset all shipping and payment methods
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);

				// Totals
				$this->load->model('setting/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
		
				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$sort_order = array();

					$results = $this->model_setting_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {
						if ($this->config->get('total_' . $result['code'] . '_status')) {
							$this->load->model('extension/total/' . $result['code']);

							// We have to put the totals in an array so that they pass by reference.
							$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
						}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);
				}

				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function edit() {
			$this->load->language('checkout/cart');
	
			$json = array();
	
			// Update
			if (!empty($this->request->post['quantity'])) {
				foreach ($this->request->post['quantity'] as $key => $value) {
					$this->cart->update($key, $value);
				}
	
				$this->session->data['success'] = $this->language->get('text_remove');
	
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				unset($this->session->data['reward']);
	
				$this->response->redirect($this->url->link('checkout/cart'));
			}
	
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	
		public function editer() {
			$this->load->language('checkout/cart');
			$json = array();
			$this->load->model('integral/integral');
			if(isset($this->request->post['integral'])) {
			    if($this->request->post['integral'] == "-1") {
	    			if($this->request->post['integral_value'] > $this->model_integral_integral->getIntegralLists()['conversion']) {
	    				$integral_value = 0;
	    			} else {
	    				$integral_value = $this->request->post['integral_value'];
	    			}
	    		} else if($this->request->post['integral'] > $this->model_integral_integral->getIntegralLists()['conversion']) {
	    			$integral_value = 0;
	    		} else {
	    			$integral_value = $this->request->post['integral'];
	    		}
			} else {
			    $integral_value = 0;
			}
			
			if(isset($integral_value)) {
				$this->model_integral_integral->add($integral_value);
			}
			
			$products = $this->cart->getProducts();
			$s_price = array();
			$days = array();
			$first_order_discount = 100;
			/*推荐用户首单*/
			$custromer_ref = $this->cart->getCustomerReferrer();//查询用户是否是推荐用户
			if($custromer_ref['referrer_id']) {
				
				$order_num = $this->cart->getOrderList();//查询用户是否为首单
				if($order_num['num'] == 0) {
					$recommend_rule = $this->cart->getRecommendRule();//查询推荐规则
					if($recommend_rule) {
						$first_order_discount = $recommend_rule['discount'];//推荐用户首单优惠折扣
					}
				}
			}
			
			/*---end---*/
			foreach ($products as $product) {
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
				} else {
					$unit_price = 0;
				}
				$s_price[$product['cart_id']] = $unit_price;
				if($product['production_time']) {
					$days[] = $product['production_time'];
				}
			}
			if($days) {
				$max_number = max($days);
			} else {
				$max_number = 0;
			}
			if($this->request->post['receipt_date']) {
				$day = strtotime($this->request->post['receipt_date']);
				$this->load->model('discount/discount');
				$discount = $this->model_discount_discount->getDiscount();
				if($discount) {
					$booking_discount = $discount['days'] + $max_number;
				} else{
					$booking_discount = 0;
				}
				if($day > strtotime(date("Y-m-d", strtotime("$booking_discount day")))) {
					if($discount['number']<0 && $discount['number'] >100) {
						$early_booking_discount = 100;
					} else {
						$early_booking_discount = $discount['number'];
					}
				} else {
					$early_booking_discount = 100;
				}
			} else {
				$early_booking_discount = 100;
			}
			$price = 0;
			// Update
			
			$sub = $this->cart->getTotal();
			$cgd = $this->cart->getCustomerGroupDiscount();
			if($cgd < 0) {$cgd = 100;}
			$sub = $sub * $cgd / 100 * $first_order_discount / 100;
			if($early_booking_discount !== 100) {$subs = $sub * $early_booking_discount / 100;} else {$subs = $sub;}
			$price = $sub - $subs;
			if (!empty($this->request->post['quantity'])) {
				foreach ($this->request->post['quantity'] as $key => $value) {
					$receipt_date = $this->cart->SelectReceiptDate($key);
					if($receipt_date['production_time']) {$receipt_date['production_time'] = 0;}
					$production_time_check = date('Y-m-d',strtotime('+'.$receipt_date['production_time'].' day'));//获取n天后零点时间
					if(strtotime($this->request->post['receipt_date']) && strtotime($production_time_check) <= strtotime($this->request->post['receipt_date'])) {
						$production_time = $this->request->post['receipt_date'];
					} else if(!strtotime($this->request->post['receipt_date']) && !strtotime($this->request->post['receipt_date'])) {
						$production_time = date("Y-m-d");
					} else {
						$production_time = $production_time_check;
					}
					
					$this->cart->update($key, $value, $production_time);
				}
				$this->session->data['success'] = $this->language->get('text_remove');
	
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				unset($this->session->data['reward']);
	
			}
			
			
			$json['sub'] = $this->currency->format($subs, $this->session->data['currency']);
			if($price > 0) {
				$json['discount'] = $this->currency->format($price, $this->session->data['currency']);
			} else{
				$json['discount'] = 0;
			}
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}

	public function remove() {
		$this->load->language('checkout/cart');

		$json = array();

		// Remove
		if (isset($this->request->post['key'])) {
			$this->cart->remove($this->request->post['key']);

			unset($this->session->data['vouchers'][$this->request->post['key']]);

			$json['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			// Totals
			$this->load->model('setting/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_setting_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get('total_' . $result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);

						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}

			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
