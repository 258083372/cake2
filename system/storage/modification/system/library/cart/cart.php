<?php
namespace Cart;
class Cart {
	private $data = array();

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->tax = $registry->get('tax');
		$this->weight = $registry->get('weight');

		// Remove all the expired carts with no customer ID
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE (api_id > '0' OR customer_id = '0') AND date_added < DATE_SUB(NOW(), INTERVAL 1 HOUR)");

		if ($this->customer->getId()) {
			// We want to change the session ID on all the old items in the customers cart
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET session_id = '" . $this->db->escape($this->session->getId()) . "' WHERE api_id = '0' AND customer_id = '" . (int)$this->customer->getId() . "'");

			// Once the customer is logged in we want to update the customers cart
			$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE api_id = '0' AND customer_id = '0' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
			foreach ($cart_query->rows as $cart) {
				
				$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart['cart_id'] . "'");
				// The advantage of using $this->add is that it will check if the products already exist and increaser the quantity if necessary.
				$this->add($cart['product_id'], $cart['quantity'], json_decode($cart['option']), $cart['recurring_id']);
			}
		}
	}

	public function getProducts() {
		$product_data = array();

		$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
		
		foreach ($cart_query->rows as $cart) {
			$stock = true;
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store p2s LEFT JOIN " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p2s.product_id = '" . (int)$cart['product_id'] . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");
			if ($product_query->num_rows && ($cart['quantity'] > 0)) {
				$option_price = 0;
				$option_points = 0;
				$option_weight = 0;
				$option_data = array();
				$layer_price = array();
				$layers = "";
				
				foreach (json_decode($cart['option']) as $product_option_id => $value) {
					$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type, po.pound_specific_option FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$cart['product_id'] . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");
					if ($option_query->num_rows) {
						if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio') {
							if(is_array($value)) {
								foreach ($value as $key => $val) {
									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name,  pov.quantity, pov.price_ratio, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$val . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
									if($cart['layer_price']) {
										$layer_price = explode(',' ,$cart['layer_price']);
									} else {
										if($option_value_query->row) {
											$layer_price = $option_value_query->row['price'];
										}
									}
									if ($option_value_query->num_rows) {
										if ($option_value_query->row['price_prefix'] == '+') {
											if($layer_price && $option_query->row['pound_specific_option'] != "1") {
											   if($layer_price[$key] * $option_value_query->row['price_ratio'] * 0.01 > $option_value_query->row['price']) {
													$price = $layer_price[$key] * $option_value_query->row['price_ratio'] * 0.01;
													$option_price += $layer_price[$key] * $option_value_query->row['price_ratio'] * 0.01;
												} else {
													$price = $option_value_query->row['price'];
													$option_price += $option_value_query->row['price'];
												}
											} else {
												$price = $option_value_query->row['price'];
												$option_price += $option_value_query->row['price'];
											}
										} elseif ($option_value_query->row['price_prefix'] == '-') {
											$option_price -= $option_value_query->row['price'];
										}
									
										if ($option_value_query->row['points_prefix'] == '+') {
											$option_points += $option_value_query->row['points'];
										} elseif ($option_value_query->row['points_prefix'] == '-') {
											$option_points -= $option_value_query->row['points'];
										}
									
										if ($option_value_query->row['weight_prefix'] == '+') {
											$option_weight += $option_value_query->row['weight'];
										} elseif ($option_value_query->row['weight_prefix'] == '-') {
											$option_weight -= $option_value_query->row['weight'];
										}
									
										if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
											$stock = false;
										}
										
										if(count($value) > 1) {
											$name = $option_query->row['name'].($key+1);
										} else {
											$name = $option_query->row['name'];
										}
									
										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => $val,
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => $option_value_query->row['option_value_id'],
											'name'                    => $name,
											'value'                   => $option_value_query->row['name'],
											'type'                    => $option_query->row['type'],
											'quantity'                => $option_value_query->row['quantity'],
											'subtract'                => $option_value_query->row['subtract'],
											'price'                   => $price,
											'price_prefix'            => $option_value_query->row['price_prefix'],
											'points'                  => $option_value_query->row['points'],
											'points_prefix'           => $option_value_query->row['points_prefix'],
											'weight'                  => $option_value_query->row['weight'],
											'weight_prefix'           => $option_value_query->row['weight_prefix'],
											'pound_specific_option'   => $option_query->row['pound_specific_option'],
											'key_num'                 => $key
										);
									}
								}
							} else {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name,  pov.quantity, pov.price_ratio, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
								if($cart['layer_price']) {
									$layer_price = explode(',' ,$cart['layer_price']);
								} else {
									if($option_value_query->row) {
										$layer_price = $option_value_query->row['price'];
									}
								}
								if ($option_value_query->num_rows) {
									if ($option_value_query->row['price_prefix'] == '+') {
										if($layer_price && $option_query->row['pound_specific_option'] != "1") {
										   if($layer_price[0] * $option_value_query->row['price_ratio'] * 0.01 > $option_value_query->row['price']) {
												$price = $layer_price[0] * $option_value_query->row['price_ratio'] * 0.01;
												$option_price += $layer_price[0] * $option_value_query->row['price_ratio'] * 0.01;
											} else {
												$price = $option_value_query->row['price'];
												$option_price += $option_value_query->row['price'];
											}
										} else {
											$price = $option_value_query->row['price'];
											$option_price += $option_value_query->row['price'];
										}
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $option_value_query->row['price'];
									}
								
									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}
								
									if ($option_value_query->row['weight_prefix'] == '+') {
										$option_weight += $option_value_query->row['weight'];
									} elseif ($option_value_query->row['weight_prefix'] == '-') {
										$option_weight -= $option_value_query->row['weight'];
									}
								
									if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
										$stock = false;
									}
								
									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $value,
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'value'                   => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $price,
										'price_prefix'            => $option_value_query->row['price_prefix'],
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],
										'weight'                  => $option_value_query->row['weight'],
										'weight_prefix'           => $option_value_query->row['weight_prefix'],
										'pound_specific_option'   => $option_query->row['pound_specific_option']
									);
								}
							}
							
							if(!$layers) {
								$layers = count($layer_price);
							}
						} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
							if(!$layer_price) {
								if($cart['layer_price']) {
									$layer_price = explode(',' ,$cart['layer_price']);
								}
							}
							foreach ($value as $key => $valid) {
								$value_name = "";
								$product_option_value_id_array = array();
								$price = 0;
								foreach ($valid as $ikey => $product_option_value_id) {
									$option_value_query = $this->db->query("SELECT pov.option_value_id, pov.quantity, pov.price_ratio, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix, ovd.name FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (pov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
									if ($option_value_query->num_rows) {
										if ($option_value_query->row['price_prefix'] == '+') {
											if($layer_price) {
												if($layer_price[$key] * $option_value_query->row['price_ratio'] * 0.01 > $option_value_query->row['price']) {
													$price += $layer_price[$key] * $option_value_query->row['price_ratio'] * 0.01;
													$option_price += $layer_price[$key] * $option_value_query->row['price_ratio'] * 0.01;
												} else {
													$price += $option_value_query->row['price'];
													$option_price += $option_value_query->row['price'];
												}
											} else {
												$price += $option_value_query->row['price'];
												$option_price += $option_value_query->row['price'];
											}
										} elseif ($option_value_query->row['price_prefix'] == '-') {
											$option_price -= $option_value_query->row['price'];
										}

										if ($option_value_query->row['points_prefix'] == '+') {
											$option_points += $option_value_query->row['points'];
										} elseif ($option_value_query->row['points_prefix'] == '-') {
											$option_points -= $option_value_query->row['points'];
										}

										if ($option_value_query->row['weight_prefix'] == '+') {
											$option_weight += $option_value_query->row['weight'];
										} elseif ($option_value_query->row['weight_prefix'] == '-') {
											$option_weight -= $option_value_query->row['weight'];
										}

										if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
											$stock = false;
										}
										if(count($value) > 1) {
											$name = $option_query->row['name'].($key+1);
										} else {
											$name = $option_query->row['name'];
										}
										$value_name .= $option_value_query->row['name'];
										$product_option_value_id_array[] = $product_option_value_id;
										if($ikey < count($valid)-1) {
											$value_name .= "、";
										}
									}
								}
								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => $product_option_value_id_array,
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => $option_value_query->row['option_value_id'],
									'name'                    => $name,
									'value'                   => $value_name,
									'type'                    => $option_query->row['type'],
									'quantity'                => $option_value_query->row['quantity'],
									'subtract'                => $option_value_query->row['subtract'],
									'price'                   => $price,
									'price_prefix'            => $option_value_query->row['price_prefix'],
									'points'                  => $option_value_query->row['points'],
									'points_prefix'           => $option_value_query->row['points_prefix'],
									'weight'                  => $option_value_query->row['weight'],
									'weight_prefix'           => $option_value_query->row['weight_prefix'],
									'price_ratio'             => $option_value_query->row['price_ratio'],
									'pound_specific_option'   => '',
									'key_num'                 => $key
								);
							}
						} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
							$option_data[] = array(
								'product_option_id'       => $product_option_id,
								'product_option_value_id' => '',
								'option_id'               => $option_query->row['option_id'],
								'option_value_id'         => '',
								'name'                    => $option_query->row['name'],
								'value'                   => $value,
								'type'                    => $option_query->row['type'],
								'quantity'                => '',
								'subtract'                => '',
								'price'                   => '',
								'price_prefix'            => '',
								'points'                  => '',
								'points_prefix'           => '',
								'weight'                  => '',
								'weight_prefix'           => '',
								'pound_specific_option'   => ''
							);
						} elseif ($option_query->row['type'] == 'num') {
							$option_value_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_option_value WHERE  product_option_id = '" . (int)$product_option_id . "'");
							$option_price += $option_value_query->row['price'] * $value;
							$option_data[] = array(
								'product_option_id'       => $product_option_id,
								'product_option_value_id' => '',
								'option_id'               => $option_query->row['option_id'],
								'option_value_id'         => '',
								'name'                    => $option_query->row['name'],
								'value'                   => $value,
								'type'                    => $option_query->row['type'],
								'quantity'                => '',
								'subtract'                => '',
								'price'                   => $option_price,
								'price_prefix'            => '',
								'points'                  => '',
								'points_prefix'           => '',
								'weight'                  => '',
								'weight_prefix'           => '',
								'pound_specific_option'   => ''
							);
						}
					}
				}
				$price = $product_query->row['price'];
				// Product Discounts
				$discount_quantity = 0;

				foreach ($cart_query->rows as $cart_2) {
					if ($cart_2['product_id'] == $cart['product_id']) {
						$discount_quantity += $cart_2['quantity'];
					}
				}

				$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

				if ($product_discount_query->num_rows) {
					$price = $product_discount_query->row['price'];
				}

				// Product Specials
				$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

				if ($product_special_query->num_rows) {
					$price = $product_special_query->row['price'];
				}

				// Reward Points
				$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($product_reward_query->num_rows) {
					$reward = $product_reward_query->row['points'];
				} else {
					$reward = 0;
				}

				// Downloads
				$download_data = array();

				$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$cart['product_id'] . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

				foreach ($download_query->rows as $download) {
					$download_data[] = array(
						'download_id' => $download['download_id'],
						'name'        => $download['name'],
						'filename'    => $download['filename'],
						'mask'        => $download['mask']
					);
				}

				// Stock
				if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $cart['quantity'])) {
					$stock = false;
				}

				$recurring_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recurring r LEFT JOIN " . DB_PREFIX . "product_recurring pr ON (r.recurring_id = pr.recurring_id) LEFT JOIN " . DB_PREFIX . "recurring_description rd ON (r.recurring_id = rd.recurring_id) WHERE r.recurring_id = '" . (int)$cart['recurring_id'] . "' AND pr.product_id = '" . (int)$cart['product_id'] . "' AND rd.language_id = " . (int)$this->config->get('config_language_id') . " AND r.status = 1 AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($recurring_query->num_rows) {
					$recurring = array(
						'recurring_id'    => $cart['recurring_id'],
						'name'            => $recurring_query->row['name'],
						'frequency'       => $recurring_query->row['frequency'],
						'price'           => $recurring_query->row['price'],
						'cycle'           => $recurring_query->row['cycle'],
						'duration'        => $recurring_query->row['duration'],
						'trial'           => $recurring_query->row['trial_status'],
						'trial_frequency' => $recurring_query->row['trial_frequency'],
						'trial_price'     => $recurring_query->row['trial_price'],
						'trial_cycle'     => $recurring_query->row['trial_cycle'],
						'trial_duration'  => $recurring_query->row['trial_duration']
					);
				} else {
					$recurring = false;
				}

				$product_data[] = array(
					'cart_id'         => $cart['cart_id'],
					'product_id'      => $product_query->row['product_id'],
					'name'            => $product_query->row['name'],
					'model'           => $product_query->row['model'],
					'shipping'        => $product_query->row['shipping'],
					'image'           => $product_query->row['image'],
					'option'          => $option_data,
					'download'        => $download_data,
					'quantity'        => $cart['quantity'],
					'minimum'         => $product_query->row['minimum'],
					'subtract'        => $product_query->row['subtract'],
					'stock'           => $stock,

					'ocm_special'      => $price != $product_query->row['price'],
        
					'price'           => ($price + $option_price),
					'total'           => ($price + $option_price) * $cart['quantity'],
					'reward'          => $reward * $cart['quantity'],
					'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $cart['quantity'] : 0),
					'tax_class_id'    => $product_query->row['tax_class_id'],
					'weight'          => ($product_query->row['weight'] + $option_weight) * $cart['quantity'],
					'weight_class_id' => $product_query->row['weight_class_id'],
					'length'          => $product_query->row['length'],
					'width'           => $product_query->row['width'],
					'height'          => $product_query->row['height'],
					'length_class_id' => $product_query->row['length_class_id'],
					'recurring'       => $recurring,
					'production_time' => $product_query->row['production_time'],
					'receipt_date'    => $cart['receipt_date'],
					'layer'           => $layers,
					'layer_price'     => $cart['layer_price']
				);
			} else {
				$this->remove($cart['cart_id']);
			}
		}
		return $product_data;
	}

	public function add($product_id, $quantity = 1, $option = array(), $recurring_id = 0, $receipt_date = "", $layer_price = "") {
		if(!$layer_price) {
			foreach ($option as $key => $option_array) {
				if(is_array($option_array)) {
					foreach ($option_array as $i => $options) {
						if(is_array($options)) {
							foreach ($options as $ikey => $options_array) {
								$query_option = $this->db->query("SELECT product_option_id FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . $options_array . "' AND pound_specific_option = '1'")->row;
								if($query_option) {
									$option_layer_price = $this->db->query("SELECT layer_price, price FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . $ikey . "' AND product_option_value_id = '" . $options_array . "'")->row;
									if($option_layer_price) {
										if($option_layer_price['layer_price']) {
											$layer_price = $option_layer_price['layer_price'];
										} else {
											$layer_price = $option_layer_price['price'];
										}
									} 
								}
								if($layer_price) {
									break;
								}
							}
						} else {
							$query_option = $this->db->query("SELECT product_option_id FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . $options . "' AND pound_specific_option = '1'")->row;
							if($query_option) {
								$option_layer_price = $this->db->query("SELECT layer_price, price FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . $i . "' AND product_option_value_id = '" . $options . "'")->row;
								if($option_layer_price) {
									if($option_layer_price['layer_price']) {
										$layer_price = $option_layer_price['layer_price'];
									} else {
										$layer_price = $option_layer_price['price'];
									}
								} 
							}
							if($layer_price) {
								break;
							}
						}
					}
				} else {
					$query_option = $this->db->query("SELECT product_option_id FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . $key . "' AND pound_specific_option = '1'")->row;
					if($query_option) {
						$option_layer_price = $this->db->query("SELECT layer_price, price FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . $key . "' AND product_option_value_id = '" . $option_array . "'")->row;
						if($option_layer_price) {
							if($option_layer_price['layer_price']) {
								$layer_price = $option_layer_price['layer_price'];
							} else {
								$layer_price = $option_layer_price['price'];
							}
						} 
					}
					if($layer_price) {
						break;
					}
				}
			}
		}
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cart WHERE api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
		if (!$query->row['total']) {
			$this->db->query("INSERT " . DB_PREFIX . "cart SET api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "', customer_id = '" . (int)$this->customer->getId() . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int)$product_id . "', recurring_id = '" . (int)$recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', receipt_date = '" . $this->db->escape($receipt_date) . "', layer_price = '" . $this->db->escape($layer_price) . "', quantity = '" . (int)$quantity . "', date_added = NOW()");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int)$quantity . "), receipt_date = '" . $receipt_date . "' WHERE api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
		}
	}

	public function update($cart_id, $quantity, $production_time = "") {
		if($production_time) {
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = '" . (int)$quantity . "', receipt_date = '" . $production_time . "' WHERE cart_id = '" . (int)$cart_id . "' AND api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = '" . (int)$quantity . "' WHERE cart_id = '" . (int)$cart_id . "' AND api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
		}
		return;
	}
	
	public function SelectReceiptDate($cart_id) {
		$query = $this->db->query("SELECT product.production_time FROM " . DB_PREFIX . "cart as cart, " . DB_PREFIX . "product as product WHERE cart.cart_id = '" . (int)$cart_id . "' AND cart.api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND cart.customer_id = '" . (int)$this->customer->getId() . "' AND cart.session_id = '" . $this->db->escape($this->session->getId()) . "' AND cart.product_id = product.product_id")->row;
		return $query;
	}

	public function remove($cart_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart_id . "' AND api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
		$query = $this->db->query("SELECT cart_id FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'")->rows;
		if(!$query) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "integral_session WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}
	}

	public function clear() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
		$query = $this->db->query("SELECT cart_id FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'")->rows;
		if(!$query) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "integral_session WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}
	}

	public function getRecurringProducts() {
		$product_data = array();

		foreach ($this->getProducts() as $value) {
			if ($value['recurring']) {
				$product_data[] = $value;
			}
		}

		return $product_data;
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}
		
		if($total < 0){ $total = 0;}
		return $total;
	}

	public function getTaxes() {
		$tax_data = array();

		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}

		return $tax_data;
	}

	public function getTotal() {
		$total = 0;
		
		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}
		
		return $total;
	}
	
	public function getCustomerGroupDiscount() {
		$query = $this->db->query("SELECT gm.discount FROM " . DB_PREFIX . "customer as cm, " . DB_PREFIX . "customer_group as gm WHERE cm.customer_id = '" . (int)$this->customer->getId() . "' and cm.customer_group_id = gm.customer_group_id")->row;
		
		if($query) {
			$total = $query['discount'];
		} else {
			$total = 100;
		}
		
		return $total;
	}
	
	public function getCustomerReferrer() {
		$query = $this->db->query("SELECT referrer_id FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$this->customer->getId() . "'")->row;
		return $query;
	}
	
	public function getOrderList() {
		$query = $this->db->query("SELECT count(od.order_id) AS num FROM " . DB_PREFIX . "order AS od, " . DB_PREFIX . "order_history AS oh WHERE od.customer_id = '" . (int)$this->customer->getId() . "' AND oh.order_id = od.order_id")->row;
		return $query;
	}
	
	public function getRecommendRule() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "referrer_rule")->row;
		return $query;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts() {
		return count($this->getProducts());
	}

	public function hasRecurringProducts() {
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {
				return false;
			}
		}

		return true;
	}

	public function hasShipping() {
		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				return true;
			}
		}

		return false;
	}

	public function hasDownload() {
		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				return true;
			}
		}

		return false;
	}
	
	public function getLayerPrice($product_id) {
		$query = $this->db->query("SELECT layer_price FROM " . DB_PREFIX . "cart WHERE api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "'")->row;
		return $query;
	}
	
	public function getOrderLayerPrice($order_id, $product_id) {
		$query = $this->db->query("SELECT layer_price FROM " . DB_PREFIX . "order_product WHERE product_id = '" . (int)$product_id . "' AND order_id = '" . $order_id . "'")->row;
		return $query;
	}
}
