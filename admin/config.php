<?php
// HTTP
define('HTTP_SERVER', 'http://www.cake.com/admin/');
define('HTTP_CATALOG', 'http://www.cake.com/');

// HTTPS
define('HTTPS_SERVER', 'http://www.cake.com/admin/');
define('HTTPS_CATALOG', 'http://www.cake.com/');

// DIR
define('DIR_APPLICATION', 'D:/phpstudy/PHPTutorial/WWW/ocake/admin/');
define('DIR_SYSTEM', 'D:/phpstudy/PHPTutorial/WWW/ocake/system/');
define('DIR_IMAGE', 'D:/phpstudy/PHPTutorial/WWW/ocake/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_CATALOG', 'D:/phpstudy/PHPTutorial/WWW/ocake/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'cake');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
