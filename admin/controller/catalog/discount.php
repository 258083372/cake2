<?php
class ControllerCatalogDiscount extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/discount');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/discount');
		
		$results = $this->model_catalog_discount->getList();
		
		if($results) {
			$data['days']    = $results['days'];
			$data['number']  = $results['number'];
			$data['id']      = $results['id'];
		}
		
		if (!isset($results['id'])) {
			$data['action'] = $this->url->link('catalog/discount/add', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('catalog/discount/edit', 'user_token=' . $this->session->data['user_token'], true);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('catalog/discount', $data));
	}
	
	public function add() {
		$this->load->language('catalog/discount');
	
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/discount');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_catalog_discount->add($this->request->post);			
			$this->response->redirect($this->url->link('catalog/discount', 'user_token=' . $this->session->data['user_token'] , true));
		}
		return;
	}
	
	public function edit() {
		$this->load->language('catalog/discount');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/discount');
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_catalog_discount->edit($this->request->post);

			$this->response->redirect($this->url->link('catalog/discount', 'user_token=' . $this->session->data['user_token'], true));
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		return;
	}
}