<?php
class ControllerMarketingIntegrationrule extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('marketing/integration_rule');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/integrationrule');

		$this->getList();
	}

	public function add() {
		$this->load->language('marketing/integration_rule');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/integrationrule');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_marketing_integrationrule->addIntegrationrule($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['minprice'])) {
				$url .= '&minprice=' .$this->request->get['minprice'];
			}

			if (isset($this->request->get['maximum'])) {
				$url .= '&maximum=' . $this->request->get['maximum'];
			}

			if (isset($this->request->get['rule_name'])) {
				$url .= '&rule_name=' . $this->request->get['rule_name'];
			}


			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('marketing/integration_rule');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/integrationrule');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_marketing_integrationrule->editIntegrationrule($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['minprice'])) {
				$url .= '&minprice=' . $this->request->get['minprice'];
			}

			if (isset($this->request->get['maximum'])) {
				$url .= '&maximum=' . $this->request->get['maximum'];
			}

			if (isset($this->request->get['rule_name'])) {
				$url .= '&rule_name=' . $this->request->get['rule_name'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('marketing/integration_rule');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/integrationrule');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_marketing_integrationrule->deleteIntegrationrule($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['minprice'])) {
				$url .= '&minprice=' . $this->request->get['minprice'];
			}

			if (isset($this->request->get['maximum'])) {
				$url .= '&maximum=' . $this->request->get['maximum'];
			}

			if (isset($this->request->get['rule_name'])) {
				$url .= '&rule_name=' . $this->request->get['rule_name'];
			}


			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		$url = '';
		if (isset($this->request->get['minprice'])) {
			$minprice = $this->request->get['minprice'];
			$url .= '&minprice=' . $this->request->get['minprice'];
		} else {
			$minprice = '';
		}

		if (isset($this->request->get['maximum'])) {
			$maximum = $this->request->get['maximum'];
			$url .= '&maximum=' . $this->request->get['maximum'];
		} else {
			$maximum = '';
		}

		if (isset($this->request->get['rule_name'])) {
			$rule_name = $this->request->get['rule_name'];
			$url .= '&rule_name=' . $this->request->get['rule_name'];
		} else {
			$rule_name = '';
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
			$url .= '&sort=' . $this->request->get['sort'];
		} else {
			$sort = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
			$url .= '&order=' . $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			$url .= '&page=' . $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('marketing/integrationrule/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('marketing/integrationrule/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['integrationrules'] = array();

		$filter_data = array(
			'minprice'          => $minprice,
			'maximum'           => $maximum,
			'rule_name'         => $rule_name,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             => $this->config->get('config_limit_admin')
		);

		$integrationrule_total = $this->model_marketing_integrationrule->getTotalIntegrationrules($filter_data);
		$results = $this->model_marketing_integrationrule->getIntegrationrules($filter_data);
		foreach ($results as $result) {
			$data['integrationrules'][] = array(
				'id'           => $result['id'],
				'minprice'         => $result['minprice'],
				'maximum'       => $result['maximum'],
				'rule_name'       => $result['rule_name'],
				'edit'         => $this->url->link('marketing/integrationrule/edit', 'user_token=' . $this->session->data['user_token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['minprice'])) {
			$url .= '&minprice=' . urlencode(html_entity_decode($this->request->get['minprice'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['maximum'])) {
			$url .= '&maximum=' . $this->request->get['maximum'];
		}

		if (isset($this->request->get['rule_name'])) {
			$url .= '&rule_name=' . $this->request->get['rule_name'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_minprice'] = $this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . '&sort=minprice' . $url, true);
		$data['sort_maximum'] = $this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . '&sort=maximum' . $url, true);
		$data['sort_rule_name'] = $this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . '&sort=rule_name' . $url, true);

		$url = '';

		if (isset($this->request->get['minprice'])) {
			$url .= '&minprice=' . urlencode(html_entity_decode($this->request->get['minprice'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['maximum'])) {
			$url .= '&maximum=' . $this->request->get['maximum'];
		}

		if (isset($this->request->get['rule_name'])) {
			$url .= '&rule_name=' . $this->request->get['rule_name'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $integrationrule_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($integrationrule_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($integrationrule_total - $this->config->get('config_limit_admin'))) ? $integrationrule_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $integrationrule_total, ceil($integrationrule_total / $this->config->get('config_limit_admin')));

		$data['minprice'] = $minprice;
		$data['maximum'] = $maximum;
		$data['rule_name'] = $rule_name;
		
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/integration_rule_list', $data));
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$url = '';

		if (isset($this->request->get['minprice'])) {
			$url .= '&minprice=' .$this->request->get['minprice'];
		}

		if (isset($this->request->get['maximum'])) {
			$url .= '&maximum=' . $this->request->get['maximum'];
		}
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['rule_name'])) {
			$url .= '&rule_name=' . $this->request->get['rule_name'];
		}
		
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('marketing/integrationrule/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('marketing/integrationrule/edit', 'user_token=' . $this->session->data['user_token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('marketing/integrationrule', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$integrationrule_info = $this->model_marketing_integrationrule->getMarketing($this->request->get['id']);
		}

		$data['user_token'] = $this->session->data['user_token'];

		$data['store'] = HTTP_CATALOG;

		if (isset($this->request->post['minprice'])) {
			$data['minprice'] = $this->request->post['minprice'];
		} elseif (!empty($integrationrule_info)) {
			$data['minprice'] = $integrationrule_info['minprice'];
		} else {
			$data['minprice'] = '';
		}

		if (isset($this->request->post['maximum'])) {
			$data['maximum'] = $this->request->post['maximum'];
		} elseif (!empty($integrationrule_info)) {
			$data['maximum'] = $integrationrule_info['maximum'];
		} else {
			$data['maximum'] = '';
		}

		if (isset($this->request->post['rule_name'])) {
			$data['rule_name'] = $this->request->post['rule_name'];
		} elseif (!empty($integrationrule_info)) {
			$data['rule_name'] = $integrationrule_info['rule_name'];
		} else {
			$data['rule_name'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/integration_rule_form', $data));
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'marketing/marketing')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}