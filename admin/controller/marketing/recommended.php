<?php
class ControllerMarketingRecommended extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('marketing/recommended');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/recommended');
		
		$results = $this->model_marketing_recommended->getList();
		
		if($results) {
			$data['reward'] = $results['reward'];
			$data['discount']    = $results['discount'];
			$data['id']       = $results['id'];
		}
		
		if (!isset($results['id'])) {
			$data['action'] = $this->url->link('marketing/recommended/add', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('marketing/recommended/edit', 'user_token=' . $this->session->data['user_token'], true);
		}
		
		$data['cancel'] = $this->url->link('marketing/recommended');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('marketing/recommended', $data));
	}
	
	public function add() {
		$this->load->language('marketing/recommended');
	
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('marketing/recommended');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_marketing_recommended->add($this->request->post);			
			$this->response->redirect($this->url->link('marketing/recommended', 'user_token=' . $this->session->data['user_token'] , true));
		}
		return;
	}
	
	public function edit() {
		$this->load->language('marketing/recommended');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/recommended');
		
		$data['cancel'] = $this->url->link('marketing/recommended');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_marketing_recommended->edit($this->request->post);

			$this->response->redirect($this->url->link('marketing/recommended', 'user_token=' . $this->session->data['user_token'], true));
		}
		
		$data['cancel'] = $this->url->link('marketing/recommended');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		return;
	}
}