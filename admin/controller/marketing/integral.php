<?php
class ControllerMarketingIntegral extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('marketing/integral');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/integral');
		
		$results = $this->model_marketing_integral->getList();
		
		if($results) {
			$data['integral'] = $results['integral'];
			$data['price']    = $results['price'];
			$data['id']       = $results['id'];
		}
		
		if (!isset($results['id'])) {
			$data['action'] = $this->url->link('marketing/integral/add', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('marketing/integral/edit', 'user_token=' . $this->session->data['user_token'], true);
		}
		
		$data['cancel'] = $this->url->link('marketing/integral');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('marketing/integral', $data));
	}
	
	public function add() {
		$this->load->language('marketing/integral');
	
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('marketing/integral');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_marketing_integral->add($this->request->post);			
			$this->response->redirect($this->url->link('marketing/integral', 'user_token=' . $this->session->data['user_token'] , true));
		}
		return;
	}
	
	public function edit() {
		$this->load->language('marketing/integral');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/integral');
		
		$data['cancel'] = $this->url->link('marketing/integral');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_marketing_integral->edit($this->request->post);

			$this->response->redirect($this->url->link('marketing/integral', 'user_token=' . $this->session->data['user_token'], true));
		}
		
		$data['cancel'] = $this->url->link('marketing/integral');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		return;
	}
}