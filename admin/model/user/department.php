<?php
class ModelUserDepartment extends Model {
	public function addDepartment($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "department SET department_name = '" . $this->db->escape($data['department_name']) . "', department_description = '" . (isset($data['department_description']) ? $this->db->escape($data['department_description']) : '') . "'");
	
		return $this->db->getLastId();
	}

	public function editDepartment($department_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "department SET department_name = '" . $this->db->escape($data['department_name']) . "', department_description = '" . (isset($data['department_description']) ? $this->db->escape($data['department_description']) : '') . "' WHERE department_id = '" . (int)$department_id . "'");
	}

	public function deleteDepartment($department_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "department WHERE department_id = '" . (int)$department_id . "'");
	}

	public function getDepartment($department_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE department_id = '" . (int)$department_id . "'");

		$department = array(
			'department_name'       => $query->row['department_name'],
			'department_description' => $query->row['department_description']
		);

		return $department;
	}

	public function getDepartments($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "department";

		$sql .= " ORDER BY department_name";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalDepartments() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "department");

		return $query->row['total'];
	}
}