<?php
class ModelCatalogDiscount extends Model {
	
	public function getList() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "early_booking_discount");
		return $query->row;
	}
	
	public function add($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "early_booking_discount SET days = '" . (int)$data['days'] . "', number = '" . (double)$data['number'] . "'");
	}
	
	public function edit($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "early_booking_discount SET days = '" . (int)$data['days'] . "', number = '" . (double)$data['number'] . "' WHERE id = '" . (int)$data['id'] . "'");
	}
}