<?php
class ModelMarketingIntegrationrule extends Model {
	public function addIntegrationrule($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "integration_rule SET minprice = '" . (float)$data['minprice'] . "', maximum = '" . (float)$data['maximum'] . "', rule_name = '" . $this->db->escape($data['rule_name']) . "'");

		return $this->db->getLastId();
	}

	public function editIntegrationrule($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "integration_rule SET minprice = '" . (float)$data['minprice'] . "', maximum = '" . (float)$data['maximum'] . "', rule_name = '" . $this->db->escape($data['rule_name']) . "' WHERE id = '" . (int)$id . "'");
	}

	public function deleteIntegrationrule($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "integration_rule WHERE id = '" . (int)$id . "'");
	}

	public function getMarketing($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "integration_rule WHERE id = '" . (int)$id . "'");

		return $query->row;
	}

	public function getMarketingByCode($code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "integration_rule WHERE code = '" . $this->db->escape($code) . "'");

		return $query->row;
	}

	public function getIntegrationrules($data = array()) {
		$implode = array();
		
		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
		}

		$sql = "SELECT * FROM " . DB_PREFIX . "integration_rule";

		$implode = array();

		if (!empty($data['minprice'])) {
			$implode[] = "minprice LIKE '" . $this->db->escape($data['minprice']) . "%'";
		}

		if (!empty($data['maximum'])) {
			$implode[] = "maximum = '" . $this->db->escape($data['maximum']) . "'";
		}
		
		if (!empty($data['rule_name'])) {
			$implode[] = "rule_name = '" . $this->db->escape($data['rule_name']) . "'";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'minprice',
			'maximum',
			'rule_name'
		);
		
		if (isset($data['sort']) && $data['sort']) {
			$sql .= " ORDER BY " . $data['sort'] . " " . $data['order'];
		} else {
			$sql .= " ORDER BY id DESC";
		}
		//if(isset($data['sort']) && ($data['minprice']))
		if (isset($data['limit'])) {
			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalIntegrationrules($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "integration_rule";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "name LIKE '" . $this->db->escape($data['filter_name']) . "'";
		}

		if (!empty($data['filter_code'])) {
			$implode[] = "code = '" . $this->db->escape($data['filter_code']) . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}