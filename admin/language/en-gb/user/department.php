<?php
// Heading
$_['heading_title']    = 'Departments';

// Text
$_['text_success']     = 'Success: You have modified departments!';
$_['text_list']        = 'Department';
$_['text_add']         = 'Add Department';
$_['text_edit']        = 'Edit Department';

// Column
$_['column_department_name']      = 'Department Name';
$_['column_department_description']      = 'Department Description';
$_['column_action']    = 'Action';

// Entry
$_['entry_department_name']       = 'Department Name';
$_['entry_department_description']      = 'Department Description';
$_['entry_access']     = 'Access Permission';
$_['entry_modify']     = 'Modify Permission';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify departments!';
$_['error_department_name']       = 'Department Name cannot be empty!';
$_['error_user']       = 'Warning: This department cannot be deleted as it is currently assigned to %s users!';