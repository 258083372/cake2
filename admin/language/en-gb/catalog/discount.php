<?php
// Heading
$_['heading_title']          = 'Early booking discount';
$_['entry_days']             = 'Booking early days';
$_['entry_number']           = 'Discount(%)';
$_['help_days']              = 'The time here is from the end of production.E.G: the order time is 1, production time to 2 days, early order discount for 7 days, then: 1+2+7=10.Discount for booking after 10th';
$_['help_number']            = 'E.G: 90%,please fill in 90';