<?php
// Heading
$_['heading_title']          = 'Categories';

// Text
$_['text_success']           = 'Success: You have modified categories!';
$_['text_list']              = 'Category List';
$_['text_add']               = 'Add Category';
$_['text_edit']              = 'Edit Category';
$_['text_default']           = 'Default';
$_['text_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Column
$_['column_name']            = 'Category Name';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Category Name';
$_['entry_description']      = 'Description';
$_['entry_meta_title']       = 'Meta Tag Title';
$_['entry_meta_keyword']     = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_store']            = 'Stores';
$_['entry_keyword']          = 'Keyword';
$_['entry_model']            = 'Model';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Location';
$_['entry_shipping']         = 'Requires Shipping';
$_['entry_manufacturer']     = 'Manufacturer';
$_['entry_date_available']   = 'Date Available';
$_['entry_quantity']         = 'Quantity';
$_['entry_minimum']          = 'Minimum Quantity';
$_['entry_stock_status']     = 'Out Of Stock Status';
$_['entry_price']            = 'Price';
$_['entry_tax_class']        = 'Tax Class';
$_['entry_points']           = 'Points';
$_['entry_option_points']    = 'Points';
$_['entry_subtract']         = 'Subtract Stock';
$_['entry_weight_class']     = 'Weight Class';
$_['entry_weight']           = 'Weight';
$_['entry_dimension']        = 'Dimensions (L x W x H)';
$_['entry_length_class']     = 'Length Class';
$_['entry_length']           = 'Length';
$_['entry_width']            = 'Width';
$_['entry_height']           = 'Height';
$_['entry_image']            = 'Image';
$_['entry_additional_image'] = 'Additional Images';
$_['entry_customer_group']   = 'Customer Group';
$_['entry_date_start']       = 'Date Start';
$_['entry_date_end']         = 'Date End';
$_['entry_priority']         = 'Priority';
$_['entry_attribute']        = 'Attribute';
$_['entry_attribute_group']  = 'Attribute Group';
$_['entry_text']             = 'Text';
$_['entry_option']           = 'Option';
$_['entry_option_value']     = 'Option Value';
$_['entry_required']         = 'Required';
$_['entrt_choose_up_layer']  = 'Choose up to a few flavors per layer';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'Categories';
$_['entry_filter']           = 'Filters';
$_['entry_download']         = 'Downloads';
$_['entry_related']          = 'Related Products';
$_['entry_tag']              = 'Category Tags';
$_['entry_reward']           = 'Reward Points';
$_['entry_layout']           = 'Layout Override';
$_['entry_recurring']        = 'Recurring Profile';
$_['entry_parent']           = 'Parent';
$_['entry_column']           = 'Columns';
$_['entry_top']              = 'Top';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Category Name must be between 1 and 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 1 and less than 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
$_['error_unique']           = 'SEO URL must be unique!';
$_['error_parent']           = 'The parent category you have chosen is a child of the current one!';