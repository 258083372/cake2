<?php
// Heading
$_['heading_title']     = 'Integral';
$_['text_form']         = 'Integral';


// Column
$_['column_integral']       = 'Integral';
$_['column_price']       = 'Price';

//help
$_['help_integral']      = 'Leave it blank if you do not need to set the scale.Integral and Price.';
$_['help_price']      = 'Leave it blank if you do not need to set the scale.Integral and Price.';

// Entry
$_['entry_integral']        = 'Integral';
$_['entry_price']        = 'Price';