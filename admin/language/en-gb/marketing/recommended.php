<?php
// Heading
$_['heading_title']     = 'Recommended Rule';
$_['text_form']         = 'Recommended Rule';

// Column
$_['column_reward']     = 'Reward';
$_['column_discount']   = 'Discount(%)';

//help
$_['help_reward']       = 'Recommender bonus points.';
$_['help_discount']     = 'Discount for Recommender first order.For example, enter 90 for 90%.';

// Entry
$_['entry_reward']      = 'Reward';
$_['entry_discount']    = 'Discount(%)';