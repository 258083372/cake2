<?php
// Heading
$_['heading_title']                    = 'Integration Rule';
$_['text_form']                        = 'Integration Rule';


// Column
$_['column_minimum_order_value']       = 'Minimum Order Value';
$_['column_maximum_discount_amount']   = 'Maximum discount amount';
$_['column_rule_name']                 = 'Rule Name';
$_['column_action']                    = 'Action';

// Entry
$_['entry_minimum_order_value']        = 'Minimum Order Value';
$_['entry_maximum_discount_amount']    = 'Maximum discount amount';
$_['entry_rule_name']                  = 'Rule Name';
$_['entry_action']                     = 'Action';